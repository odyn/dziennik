#coding: utf-8
from django.db import models

# Create your models here.


class StudentGroup(models.Model):
    "grupa studentów"
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name

class Student(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=80, db_index=True)
    identity = models.CharField(max_length=20, db_index=True)
    email = models.EmailField(blank=True)
    group = models.ForeignKey(StudentGroup)

    def __unicode__(self):
        return "%s %s" %(self.first_name, self.last_name)


class Subject(models.Model):
    "prowadzony przedmiot"
    name = models.CharField(max_length=128)
    groups = models.ManyToManyField(StudentGroup)
    #students = models.ManyToManyField(Student)

    def __unicode__(self):
        return self.name


class Event(models.Model):
    "pojedyncze spotkanie"
    subject = models.ForeignKey(Subject)
    date = models.DateTimeField()
    students = models.ManyToManyField(Student)

    def __unicode__(self):
        return "%s %s" % (self.subject, self.date)

class GradeCategory(models.Model):
    "kategoria oceny - za co"
    name = models.CharField(max_length=120)
    max_value = models.PositiveIntegerField()

    def __unicode__(self):
        return self.name

class Grade(models.Model):
    "ocena za coś"
    value = models.PositiveSmallIntegerField()
    category = models.ForeignKey(GradeCategory)
    student = models.ForeignKey(Student)
    subject = models.ForeignKey(Subject)

    def __unicode__(self):
        return "%(value)s %(person)s for %(category)s" % {
            "value": self.value,
            "person": self.student,
            "category": self.category
        }

###
# student

# zajęcia (przedmiot)
# zajęcia (konkretne pojedyncze wydarzenie)
# grupa
# ocena
# kategoria oceny (za co)
# obecność
#
