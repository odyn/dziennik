#coding: utf-8
from django.contrib import admin

from .models import Student, StudentGroup, Event, Subject, GradeCategory, Grade
# Register your models here.


admin.site.register([Student, StudentGroup, Event, Subject, GradeCategory, Grade])
#admin.site.register(Student, StudentAdmin )
